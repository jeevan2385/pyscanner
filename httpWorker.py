from multiprocessing import  Queue
import multiprocessing
from utilities import QueueSize
from dbHandler import dbHandler
import logging
import sys
import time
import requests

logger = logging.getLogger('HTTPWorker')
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(threadName)s@@%(thread)d - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class httpWorker(multiprocessing.Process):
  def __init__(self, dbQueue,httpQueue,parentQueue):
    logger.debug("beginning")
    super(httpWorker,self).__init__()
    self.dbQueue = dbQueue
    self.httpQueue = httpQueue
    self.parentQueue = parentQueue
    #self.setDaemon(True)

  def run(self):
    logger.debug("run - beginning")
    ip = self.httpQueue.get() #blocking call till item is available

    while ip is not None: #this is exit condition
      # extract emails on ip page
      emails = getIpEmails(ip)
      logger.info("{} : {}".format(ip,emails))
      #mark ip processed
      self.dbQueue.put(("saveEmails",ip,emails))
      #self.httpQueue.task_done()
      #add to parent queue to ask for another ip
      self.parentQueue.put(self.httpQueue)
      ip = self.httpQueue.get() #blocking call till item is available

    #self.httpQueue.task_done() # this is for the None
    logger.debug("run - EXITTING")
  
  def addIp(self,ip):
    self.httpQueue.put(ip)

#---- Given an ip fetch the web page and extract contact email id if found -----
#---- IP Should be a string ----
def getIpEmails(IP):
  URL = "http://" + IP
  try:
    R = requests.get(URL,verify=False,allow_redirects=True,timeout=2)
    Matches = re.findall("(mailto:.*)[\"|']>",R.text)
    return extractEmails(Matches)
  except:
    return []

#--- Extract emails from a list of strings ---
def extractEmails(listOfStrings):
  Emails = []
  for String in listOfString:
    Matches = re.findall("[\w\.-]+@[\w\.-]+\.\w+",String)
    Emails += Matches
  return Emails

if __name__ == "__main__":
  #do nothing
  mp = multiprocessing.Manager()
  q1 = mp.Queue()
  q2 = mp.Queue()
  q3 = mp.Queue()
  worker = httpWorker(q1,q2,q3)
  worker.addIp("1.2.3.4")
  worker.start()
  worker.join()
