What Is It?

Given a range of Ip-Addresses, fetches html page hosted on each Ip and extract "mailto:" information.

Requirements : Python3, sqlite3

Modules : requirements.txt

Start : python scanner.py --workers 10

This will load the data and create a sqlite database : ipemail.db

Stop : Ctrc-c or create following entry in table "setting" of database ipemail.db
       insert into setting values("stopScanner","stop");
       OR
       update setting set value = "stop" where name = "stopScanner";
