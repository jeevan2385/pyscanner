import datetime
import time

import sys

from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Table, ForeignKey, text
from sqlalchemy.types import String, Integer, TIMESTAMP, Float
from datetime import datetime
from models.tables import *

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class dbHandler(metaclass=Singleton):

  _session = None
  _db = None
  _db_location = None
  

  def __init__(self,db_location=None):
    super(dbHandler,self).__init__()
    if self._db is None:
      if db_location and os.path.exists(db_location):
        self._db_location = "sqlite:///{}ipemails.db".format(db_location)
      else:
        self._db_location = "sqlite:///ipemails.db"
      self.connect()

  @property
  def session(self):
    if not self._session:
      self.connect()
    return self._session

  def connect(self):
    from sqlalchemy import create_engine, orm

    if self._db is None:
      print("creating engine")
      self._db = create_engine(self._db_location,connect_args={'timeout':5})
      self._db.echo = False # set this to True to watch db logs

    if self._session:
      self._session.close()
      #hopefully session will keep the thread locality in place
    self._session = orm.scoped_session(orm.sessionmaker(bind=self._db, autocommit=True))

    from models.tables import Base
    import models.tables
    Base.metadata.create_all(bind=self._db)
    self.seedRanges()

  def disconnect(self):
    self.session.close()
    self._db.dispose()

  #------- APIs --------
  def seedRanges(self):
    ExistingRecords = self.session.query(IpNetwork).count()
    if ExistingRecords < 1 :
      print("Loading Data... This may take some time.")
      import ipaddress
      for line in open("ranges.csv","r"):
        Range = line.strip()
        Network = ipaddress.ip_network(Range)
        Addresses = Network.num_addresses
        NetworkExists = self.session.query(IpNetwork
                                   ).filter_by(network = Range
                                   ).first()
        if not NetworkExists:
          self.session.execute(IpNetwork.__table__.insert(),
                               {"network":Range,"addresses":Addresses,"status":"unprocessed"})
    
  def getAllIpEmails(self):
    return self.session.query(IpEmail).count()

  def saveEmails(self,Ip,Emails):
    ipExists = self.session.query(IpEmail
                          ).filter_by(ip=Ip
                          ).first()
    if not ipExists:
      self.session.execute(IpEmail.__table__.insert(),
                           {"ip":Ip,"email":",".join(Emails)})

  def ipExists(self,Ip):
    return self.session.query(IpEmail
                      ).filter_by(ip=Ip
                      ).first()

  def setNetworkProcessed(self,Network):
    networkEntry = self.session.query(IpNetwork
                              ).filter_by(network=Network
                              ).first()
    networkEntry.status = 'processed'

  def getNextNetwork(self):
    network = self.session.query(IpNetwork
                         ).filter_by(status='unprocessed'
                         ).order_by(IpNetwork.addresses
                         ).first()
    if not network:
      return None
    else:
      return network.network

  def getSetting(self,Name):
    setting = self.session.query(Setting
                         ).filter_by(name=Name
                         ).first()
    if setting:
      return setting.value
    else:
      return None

if __name__ == "__main__":
  D = dbHandler()
  D.saveEmails("1.2.3.4",["hello@world.com","foo@bar.com"])
  D.getAllIpEmails()
  D.disconnect()
