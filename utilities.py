from datetime import date,datetime
from gettext import NullTranslations
import linecache


def PrintException():
    import time
    import sys
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print('{} EXCEPTION IN ({}, LINE {} "{}"): {}'.format(time.ctime(), filename, lineno, line.strip(), exc_obj))

def object_to_dict(_object):
    return dict((key, value) for key, value in _object.__dict__.iteritems() if not callable(value) and not key.startswith('__'))

def QueueSize(queue):
  try:
    return queue.qsize()
  except:
    return 0

def naturalday(value, arg=None, birthday=False):
    """
    For date values that are tomorrow, today or yesterday compared to
    present day returns representing string. Otherwise, returns a string
    formatted according to settings.DATE_FORMAT.
    """
    try:
        value = date(value.year, value.month, value.day)
    except AttributeError:
        # Passed value wasn't a date object
        return value
    except ValueError:
        # Date arguments out of range
        return value
    today = datetime.utcnow().date()
    delta = value - today
    if delta.days == 0:
        return 'Today'
    elif delta.days == 1:
        return 'Tomorrow'
    elif delta.days == -1:
        return 'Yesterday'
    elif birthday:
        return value.strftime('%d %b')
    else:
        return value.strftime('%d %b %Y')

def naturaltime(value):
    """
    For date and time values shows how many seconds, minutes or hours ago
    compared to current timestamp returns representing string.
    """
    if not isinstance(value, date): # datetime is a subclass of date
        return value
    now = datetime.utcnow()
    ungettext = NullTranslations().ungettext
    if value < now:
        delta = now - value
        if delta.seconds == 0:
            return 'just now'
        elif delta.seconds < 60:
            return ungettext(
                'a second ago', '%(count)s seconds ago', delta.seconds
            ) % {'count': delta.seconds}
        elif delta.seconds // 60 < 60:
            count = delta.seconds // 60
            return ungettext(
                'a minute ago', '%(count)s minutes ago', count
            ) % {'count': count}
        else:
            count = delta.seconds // 60 // 60
            return ungettext(
                'an hour ago', '%(count)s hours ago', count
            ) % {'count': count}

