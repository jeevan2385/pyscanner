import multiprocessing
from utilities import QueueSize
from dbHandler import dbHandler
import logging
import sys
import time

logger = logging.getLogger('DB_Writer')
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(threadName)s@@%(thread)d - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class dbWriter(multiprocessing.Process):
  def __init__(self, queue):
    logger.debug("beginning")
    super(dbWriter,self).__init__()
    self.queue = queue
    #self.setDaemon(True)

  def actual_dispatch(self, method, *args):
    #for count, thing in enumerate(args):
    #  print('actual_dispatch - {0}. {1} - {2}'.format(count, thing, msg))

    logger.debug("db writer method %s" % str(method) )
    try:
      d = dbHandler()
      actual_method = getattr(d,method)
      args_len = len(args)
      if args_len == 1:
          response = actual_method(args[0])
      elif args_len > 1:
          response = actual_method(*args)
      else:
          response = actual_method()
    # we do this because there may be multiple weakmethods and one of them might be dead
    except BaseException as e:
      logger.info("Error occured in db writer: %s"%method)
      logger.info(args)
      logger.info(str(e))

  def run(self):
    logger.debug("running")
    event = self.queue.get() #blocking call till item is available

    while event is not None: #this is exit condition
      #do something
      logger.debug("event %s" % str(event) )
      self.actual_dispatch(*event)
      #self.queue.task_done()
      event = self.queue.get() #blocking call till item is available
    #self.queue.task_done() # this is for the None
    logger.debug("run - EXITTING")

  def stop(self):
    logger.debug(" join - trying" )
    if self.queue:
      logger.debug(" join - inserting None" )
      self.queue.put(None)

  def write(self,*args):
    """push a string inside the queue """
    logger.debug("before queue size %s" % QueueSize(self.queue) )
    try:
        self.queue.put(args)
    except BaseException as e:
        logger.info("Error occured in db write put: %s"%str(e))
        logger.info(args)
    logger.debug("after queue size %s" % QueueSize(self.queue) )


if __name__ == "__main__":
  #do nothing
  m = multiprocessing.Manager()
  #q = multiprocessing.Queue()
  #q = multiprocessing.JoinableQueue()
  q = m.Queue()
  bus = dbWriter(q)
  bus.write("getAllIpEmails")
  bus.start()
  bus.stop()
  bus.join()
