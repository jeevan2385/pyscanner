from multiprocessing import  JoinableQueue,Queue
import multiprocessing
from dbWriter import dbWriter
from httpWorker import httpWorker
from ipGenerator import ipGenerator
import logging
import sys

logger = logging.getLogger('SCANNER')
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(threadName)s@@%(thread)d - %(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

class scanner(multiprocessing.Process):

  def __init__(self,httpWorkersNumber):
    #logger.info("init scanner")
    super(scanner,self).__init__()
    #---- can't use JoinableQueue here directly, they are shared using inheritence -----
    self.httpWorkersNumber = httpWorkersNumber
    manager = multiprocessing.Manager()
    self.dbQueue = manager.Queue()
    self.httpQueue = manager.Queue()
    dbwriter = dbWriter(self.dbQueue)
    dbwriter.start()
    self.ipGenerator = ipGenerator(self.dbQueue)
    self.processes = [self,dbwriter]

  def run(self):
    #logger.info("starting scanner")
    manager = multiprocessing.Manager()
    for i in range(self.httpWorkersNumber):
      httpQueue = manager.Queue()
      httpworker = httpWorker(self.dbQueue,httpQueue,self.httpQueue)
      httpworker.start()
      self.httpQueue.put(httpQueue)
      self.processes.append(httpworker)

    #---- While Testing ----
    #self.httpQueue.put(None)
      
    queue = self.httpQueue.get()
    while queue is not None:
      nextIp = self.ipGenerator.getNextIp()
      #logger.debug("nextip : {}".format(nextIp))
      queue.put(nextIp)
      if nextIp is None:
        self.httpWorkersNumber -= 1
      if self.httpWorkersNumber > 0:
        queue = self.httpQueue.get()
      else:
        queue = None

    #------ Stop DB ----
    self.dbQueue.put(None)

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("-w", "--workers", type=int,
                      help="number of http workers")
  args = parser.parse_args()
  workers = args.workers or 1
  S = scanner(workers)
  S.start()
  for process in S.processes:
    process.join()
