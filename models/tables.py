from sqlalchemy.types import String, Integer, TIMESTAMP, DATE,Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy.types import String, Integer, TIMESTAMP, DATE,Boolean
from sqlalchemy import func

class DBBase(object):

  def __tablename__(self):
    return self.__name__.lower()
  id = Column(Integer, primary_key=True)
  created_time = Column(TIMESTAMP, server_default=func.now())
  modified_time = Column(TIMESTAMP, server_default=func.now(), onupdate=func.now())

#Base = declarative_base(cls=DBBase)
Base = declarative_base()

class IpEmail(Base):
  __tablename__ = 'ipemail'

  ip       = Column(String(length=20), primary_key=True)
  email    = Column(String(length=1000), nullable=True)
  
  def __repr__(self):
      return "<IpEmail ('%s'), ('%s')>" % ( self.ip,
                                            self.email,
                                          )
class IpNetwork(Base):
  __tablename__ = 'ipnetwork'

  network       = Column(String(length=20), primary_key=True)
  addresses     = Column(Integer,nullable=False)
  status        = Column(String(length=100),nullable=True)

class Setting(Base):
  __tablename__ = 'setting'

  name  = Column(String(length=100),primary_key=True)
  value = Column(String(length=100),nullable=False)
