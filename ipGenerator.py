from dbHandler import dbHandler
import ipaddress

class ipGenerator(object):
  
  _current_network = None
  _network_generator = None
  
  def __init__(self,dbQueue):
    super(ipGenerator,self).__init__()
    self.dbQueue = dbQueue
    self.dbHandler = dbHandler()
    self.setNetwork()

  def getNextIp(self):
    shouldStop = self.dbHandler.getSetting("stopScanner")
    if shouldStop == 'stop':
      return None
    if self._current_network is None:
      return None
    if self._network_generator is not None:
      try:
        Ip = str(self._network_generator.__next__())
        Exists = self.dbHandler.ipExists(Ip)
        while Exists:
          Ip = str(self._network_generator.__next__())
          Exists = self.dbHandler.ipExists(Ip)
        return Ip
      except:
        self.dbQueue.put(("setNetworkProcessed",self._current_network))
        self.setNetwork()
        self.getNextIp()
  
  def setNetwork(self):
    self._current_network = self.dbHandler.getNextNetwork()
    #---- following is a generator ---
    if self._current_network:
      self._network_generator = ipaddress.ip_network(self._current_network).hosts()
    
if __name__ == '__main__':
  import multiprocessing
  Manager = multiprocessing.Manager()
  dbQ = Manager.Queue()
  ipG = ipGenerator(dbQ)
  print("next ip : {}".format(ipG.getNextIp()))
